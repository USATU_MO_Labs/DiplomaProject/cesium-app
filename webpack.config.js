const path = require('path');
const webpack = require('webpack');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopywebpackPlugin = require('copy-webpack-plugin');
const BundleTrackerPlugin = require('webpack-bundle-tracker');

// The path to the Cesium source code
const cesiumSource = 'node_modules/cesium/Source';
const cesiumWorkers = '../Build/Cesium/Workers';

module.exports = {
  mode: 'development',

  context: __dirname,

  entry: './src/index.js',
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist'),

    sourcePrefix: '',
  },

  amd: {
    // Enable webpack-friendly use of require in Cesium
    toUrlUndefined: true,
  },

  node: {
    // Resolve node module use of fs
    fs: 'empty',
  },

  // Enable sourcemaps for debugging webpack's output.
  devtool: 'eval-source-map',

  resolve: {
    alias: {
      // костыль, чтобы можно было использовать @types/cesium
      // @types/cesium содержит тайпинги для cesium/index.js в Cesium, но в инструкциях по
      // связыванию Webpack и Cesium (https://cesiumjs.org/tutorials/cesium-and-webpack/)
      // нужно использовать код из cesium/Source. Если использовать index.js,
      // Webpack откажется это кушать.
      // cesium -> cesium/Source/Cesium.js
      cesium$: path.resolve(__dirname, cesiumSource, 'Cesium'),

      // cesium/Widgets -> cesium/Source/Widgets
      'cesium/Widgets': path.resolve(__dirname, cesiumSource, 'Widgets'),
    },

    // Add '.ts' and '.tsx' as resolvable extensions.
    extensions: ['.js', '.json'],
  },

  module: {
    // avoid warnings from Cesium pulling in some third-party AMD-formatted modules like Knockout
    unknownContextCritical: false,

    rules: [
      // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
      {
        enforce: 'pre',
        test: /\.js$/,
        loader: 'source-map-loader',
      },

      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },

      {
        test: /\.(png|gif|jpg|jpeg|svg|xml)$/,
        use: ['url-loader'],
      },

      { test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader' },
    ],
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: 'src/index.html',
    }),

    // Copy Cesium Assets, Widgets, and Workers to a static directory
    new CopywebpackPlugin([
      {
        from: path.join(cesiumSource, cesiumWorkers),
        to: 'Workers',
      },
    ]),
    new CopywebpackPlugin([
      {
        from: path.join(cesiumSource, 'Assets'),
        to: 'Assets',
      },
    ]),
    new CopywebpackPlugin([
      {
        from: path.join(cesiumSource, 'Widgets'),
        to: 'Widgets',
      },
    ]),

    new webpack.DefinePlugin({
      // Define relative base path in cesium for loading assets
      CESIUM_BASE_URL: JSON.stringify(''),
    }),

    // Нужно, чтобы подружить Webpack с Django (см. https://owais.lone.pw/blog/webpack-plus-reactjs-and-django/)
    new BundleTrackerPlugin({ filename: './webpack-stats.json' }),
  ],

  // development server options
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
  },
};
