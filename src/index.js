import Cesium from 'cesium';

// styles import
// eslint-disable-next-line import/no-unresolved
import 'cesium/Widgets/widgets.css';
import './index.css';

// mock points
import points from './points.json';

const viewer = new Cesium.Viewer('cesiumContainer');

points.forEach((item) => {
  const { longtitude, latitude, elevation: height } = item;

  viewer.entities.add({
    point: {
      color: Cesium.Color.WHITE,
    },

    position: Cesium.Cartesian3.fromDegrees(longtitude, latitude, height),
  });
});
